var mysql = require('mysql'),
    express = require('express')

var DB = {};


DB.config = {
	development : {username:'root', password:'rtdp6186'},
	development_db : 'github_node_development',
	production : {username : 'root', password:'p@ssw0rd'},
	production_db : 'github_node_production',
	db_created : false,
	table_created : false,
	room_project_table_created : false,
	
	get_database : function(){
		if(process.env.NODE_ENV == 'production')
			return DB.config.production_db;
		else
			return DB.config.development_db;		
	}
	
};


DB.connections = function(){
	this.client;
	if(process.env.NODE_ENV == 'production'){
		this.client   = mysql.createClient(DB.config.production);			
	}
	else
		this.client   = mysql.createClient(DB.config.development);	

		
	
	this.switch_database =  function(database){
		if(!database)
			database = DB.config.get_database();
			
			this.client.useDatabase(database, function(data){
				// console.log(data);
			});
	};
	
	this.exec_query = function(query_string, callback){			
		var query_results;
		var self = this;
		this.client.query(query_string, function(err, results, fields){
			 console.log(query_string);
			// console.log(results);
			// console.log(fields);
			if(err){
				console.log(err.message);
				console.log(err.stack);
				throw err;
			}
				
			if(callback){								
				callback(results, fields);
				return;
			}
				
			
		});
		
		return query_results;
	};	
	
	this.init = function(){
		if(!DB.config.db_created){			
			this.exec_query('create database IF NOT EXISTS ' + DB.config.get_database(), function(results, field){
				if((results.affectedRows == 1) && (results.serverStatus == 2))
					DB.config.db_created = true;
			});			
		}
			
		if(!DB.config.table_created){			
			this.switch_database();
			this.exec_query('create table IF NOT EXISTS user_tokens (id INT NOT NULL AUTO_INCREMENT PRIMARY KEY, user_id INT, username VARCHAR(255), password VARCHAR(255), token VARCHAR(255))', function(results, fields){
				if((results.affectedRows == 0) && (results.serverStatus == 2))
					DB.config.table_created = true;					
			});				
		}
		
		if(!DB.config.room_project_table_created){
			this.switch_database();
			this.exec_query('create table IF NOT EXISTS room_projects (id INT NOT NULL AUTO_INCREMENT PRIMARY KEY, room_id INT, project_id VARCHAR(255))', function(results, fields){
				if((results.affectedRow == 0) && (results.serverStatus == 2))
					DB.config.room_project_table_created = true;
			});
		}
			
	};
	
	this.close_connection = function(){
		this.client.end();
	}
	
}


module.exports = DB;