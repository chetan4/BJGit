// write all your startup tasks in this.
// Similar to rake db:seed

if(process.argv[2] == 'production')
	process.env.NODE_ENV = 'production';
else
	process.env.NODE_ENV = 'development';
	
var DB = require('../config/db');


var connection = new DB.connections();
connection.init();
connection.close_connection();
