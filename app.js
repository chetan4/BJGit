
/**
 * Module dependencies.
 */

  var express = require('express')
  , routes    = require('./routes')
  , GithubClient    = require('./lib/github_client')
  , GithubParser    = require('./lib/github_parser')
  , Helper = require('./lib/helpers.js')
  , winston = require('winston')
  , util = require('util')
  , github_client = new GithubClient()

var app = module.exports = express.createServer();

// Configuration

app.configure(function(){
  app.set('views', __dirname + '/views');
  app.set('view engine', 'jade');
  app.use(express.bodyParser());
  app.use(express.methodOverride());
  app.use(app.router);
  app.use(express.static(__dirname + '/public'));
});

app.configure('development', function(){
  app.use(express.errorHandler({ dumpExceptions: true, showStack: true })); 
});

app.configure('production', function(){
  app.use(express.errorHandler()); 
});

// Routes

app.get('/', routes.index);

// User registration
app.get('/users', function(req, res, next){
  var username      = req.query.github_username;
  var password      = req.query.github_password;
  var uid                 = req.query.uid;
  var callback         = req.query.callback;  

  github_client.register_user(username, password, uid, function(registration_status, msg){   
    var response     = {};
    response.status  = registration_status;
    response.message = msg;
    console.log(response);
    
    res.contentType('application/json');
    var r = callback + "(" + JSON.stringify(response)  + ");";
    res.send(r);
  }); 
});

app.get('/api', function(req, res, next){
  if((!req.query.callback) || (!req.query.data) || (!req.query.data.command) || (!req.query.data.commander) || (!req.query.data.room_id) || (!req.query.data.message_id))
    throw new Exceptions.InSufficientArguments;
    
    var callback                    = req.query.callback;
    var command                 = req.query.data.command;
    var commander             = req.query.data.commander;
    var room_id                   = req.query.data.room_id;
    var message_identifier  = req.query.data.message_id;
    var project_id                 = req.query.data.project_id;

  winston.info("INside getting the query");
  winston.info(req.query);
  winston.log('debug', "Query data", req.query)

  GithubParser.parse_command(command, message_identifier, function(method, arguments){
    var github_client = new GithubClient();   
    //console.log(arguments);
    
    try{
      github_client[method](arguments, commander, room_id, function(resp){     
        var response = Helper.generate_default_response(message_identifier, command, commander);
        response.data    = resp.data;
        response.status  = resp.status;       
        
        console.log('-----------------------------');
        console.log(response);
        console.log('-----------------------------');
        
        if(resp.message)
          response.message = resp.message;        
          
        res.contentType('application/javascript');
        var r = callback + "(" + JSON.stringify(response)  + ");";
        res.send(r);  
      });
      
    }
    catch(err){
      console.log(err.message);
      console.log(err.stack);
      winston.log('error', err);
      
      var response     = Helper.generate_default_response(message_identifier, command, commander);
      response.status  = 'error';
      if(err.message.indexOf('is not a function') > -1)
      {
        response.message = "Invalid command";
        response.data = ['Invalid command.'];
      }       
        
      else
      {
        response.message = err.message;
        response.data = [err.message];
      }
      
      var r = callback + "(" + JSON.stringify(response)  + ");";
      res.send(r);    
      
    }
  });
});

if(app.settings.env == 'development')
	app.listen(3001);
else
	app.listen(9001);
	
console.log("Express server listening on port %d in %s mode", app.address().port, app.settings.env);

