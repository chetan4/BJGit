var Helper = require('./helpers')

var GithubParser = {
  parse_command : function(command, message_identifier, callback){
    console.log('***************************************')
    console.log(command);
    console.log(message_identifier) 
    console.log('***************************************')
    var split_command = command.match(/\w+|"[^"]+"/g);  //command.split(' ');
    var method_name   = split_command.splice(0,1);
    var arguments     = split_command;
    
    callback(method_name, arguments);   
  },
  
  // commands format
  // "owned_by=siddharth,estimate=3,description=Add tests to all modules,tags=show_stopper"
  parse_basejumper_command : function(commands)
  { var operations     = {};

    if(commands){     
      var split_commands = commands.split(',');
      split_commands.forEach(function(operation){
        var key_values = operation.split('=');
        var key        = key_values[0].replace(/"/g, "").replace(/(^\s*$)|(\s*$)/gi,""); // removes the extra double quotes and leading and trailing
        var value      = key_values[1].replace(/"/g,"").replace(/(^\s*$)|(\s*$)/gi,""); // whitespaces.       

        operations[key] = value;
      });     
    }
    
    return operations;
  },
  
  trim : function(arg){
    var response;
    
    if(typeof arg == 'string' )
    {
      response = arg.replace(/(^\s*$)|(\s*$)/gi,"")
    }
    else
      response = arg.toString();      

    return response;
  },  
  
  remove_double_quotes : function(arg){
    var response = arg;
    if(typeof arg == 'string')
    {
      response = arg.replace(/"/g,"");
    }
    
    return response;
  }
}

module.exports = GithubParser;

