Helper = {
  trim: function(s) {
    s    = s.replace(/(^\s*)|(\s*$)/gi,"");
    s    = s.replace(/[ ]{2,}/gi," ");
    s    = s.replace(/\n /,"\n");
    return s;
  },
  
  generate_default_response : function(message_identifier, command, commander, data, data_type){
    var response               = {};
    response.status            = "success";
    response.bot_id            = 2;
    response.command_timestamp = message_identifier;
    response.important         = true;
    
    if(command)
      response.command    = command;

    if(message_identifier)
      response.message_id = message_identifier;

    response.timestamp  = new Date().getTime();

    if(commander)
      response.commander  = commander;


    if(data_type)
      response.data_type  = data_type;
    else
      response.data_type  = "json";

    if(data)  
      response.data       = data;

    return response;
  },
  
  
  is_empty_object : function(obj)
  {
    for(var prop in obj)
    {
      if(obj.hasOwnProperty(prop))
        return false;
    }   
    
    return true;
  }
};


module.exports = Helper;
