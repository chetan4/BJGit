var https = require('https')
  ,http = require('http')
  ,sys = require('sys')
  ,DB  = require('../config/db')
  ,query_string = require('querystring')
  // ,parser = require('xml2json')
  ,util = require('util')
  // ,winston = require('winston')
  // ,Helper = require('./helpers')
  // ,PivotalParser = require('./bot_parser.js')
  // ,Extend = require('./extend')
  ,check = require('validator').check
  ,sanitize = require('validator').sanitize
  
  
  // if(process.env.NODE_ENV == 'production') 
  //  winston.add(winston.tranports.File, {filename: 'logs/production.log', level:'debug'});
  // else
  //  winston.add(winston.transports.File, {filename: 'logs/development.log', level:'info'});

  var github_client = function()
  {
    var self  = this;
    
    this.register_user = function(username, password, uid, callback){
      var register_status = false;
      
      if((!uid) || (!username) || (!password))                           // return false if these parameters aren't provided.
        return register_status;

      // Make Request 
      try{
        self.update(uid, username, password, callback);
      }
      catch(err)
      {
        console.log('---------- INSIDE CATCH BLOCK -----------------');
        console.log(err.toString());
        console.log(err.stack);
        callback(register_status, chunk);
      }

    };
  
  
  this.update = function(uid, username, password, callback)
  {
    // Use response to update database
      //check if the record exists
      var db = new DB.connections();
      db.switch_database();
      db.exec_query("SELECT * from user_tokens WHERE user_id = " + uid, function(result, fields)
      {
        if(result.length == 0)
        {
          db.exec_query('INSERT into user_tokens (user_id, username, password) VALUES (' +  uid + ', "' + username + '", "' + password + '")', function(result, fields)
          {
            if(result.affectedRows == 1)
            {
              register_status = true;
              callback(register_status)
            }
          });
          // end of INSERT callback
        }
        else        
        {           
          if(((result[0].user_id == uid) && (result[0].password == password)))
          {
            register_status = true;
            callback(register_status);
            // end of UPDATE callback
          }
          else
          {
            db.exec_query('UPDATE user_tokens SET password="' + password + '", username = "' + username + '" WHERE user_id = ' + uid, function(result, fields)
            {
              if(result.affectedRows == 1)
              {
                register_status = true;
                callback(register_status);
              }
            });           
          }
        }
      });
      // end of SELECT callback
  };
  
  this.help = function(args, uid, room_id, callback)
  {
    var list_data      = {}
    list_data.elements = []


    var item1        = {};      
    item1.title       = 'Add Comment';
    item1.description = 'Adds comment to a story';
    item1.sample1     = '@pt add_comment <story_id> <comment_text> [<project_id>]';
    item1.sample2     = '@pt add_comment 1231221 "Please update the metatags with the new list."';
    
    var item2         = {};     
    item2.title       = 'Get Projects'; 
    item2.description = 'Returns all projects';
    item2.sample      = '@pt get_projects'

    var item3         = {};
    item3.title       = 'Get Stories';
    item3.description = 'Returns all stories for a given project.';
    item3.sample1     = '@pt get_stories [<project_id>]';
    item3.sample2     = '@pt get_stories none 1221131';
    item3.sample3     = '@pt get_stories "type:bug"';
  
    
    list_data.elements.push(item1, item2, item3); //, item4, item5, item6);            
    callback({data:list_data, status:'success', message:null});
    return;   
  }
  
  
  
  
  this.test = function(){
    console.log(process.env.NODE_ENV);
    console.log(mysql);
  }
  
  
};



module.exports = github_client;